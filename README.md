# BlockChain Sparrows
**The coin info provider you can trust**<br/>

[![Platform: Android](https://img.shields.io/badge/platform-android-green)]()
[![Website: Up](https://img.shields.io/badge/website-up-brightgreen)](https://www.blockchainsparrows.com)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow)](https://opensource.org/licenses/MIT)

<img src="proposal/mockups/login-sparrows.png" height="500">
<img src="proposal/mockups/prices-sparrows.png" height="500">

#### This app is for anyone with an interest in Cryptocurrency

---
### Activities Description:

#### First activity Log-In (Launcher Activity) + Main Menu + Register
The first screen for thr application will be a login screen where a username and password will be authenticated using Google's Firebase database. User will need to enter a username and password in order to access the application features. User will be able to register a username and a password that will be registered within the database. For navigation, the app will use the drawer navigation to redirect the user to different activities within the application.

#### Buy Crypto: 
This activity will enable users to buy cryptocurrency. Users will be able to choose the desired coins via a dropdown spinner, the page contains TextViews for prices and quantity, and a Button that gives you three options: One for banking information, one for the companys customer support, last lastly, too add bitcoin account number. This address will be saved within the firebase database.

#### Signal Subscription: 
In this activity, the user will be able to subscribe by directing the user directly to the BlockChain Sparrows's service on the [Zignaly](https://zignaly.com) bot. The layout will contain CardViews within a RecyclerView and will contain buttons that enable the subscriptions. When the user clicks the button for Academy CardView, users will subscribe to Blockchain Sparrows YouTube channel. The layout also contains ImageViews and TextViews.

#### Announcements:
The annoucements activity is like the news activity, but users will receive annoucements directly from the Blockchain Sparrows API. The layout will contain CardViews within a RecyclerView.

#### Coin Prices:
For the prices activity, the application will be receiving data from the [CoinMarketCap](https://coinmarketcap.com/api/) API to display coin names, prices, percentages, etc. This activity's layout will contain TextViews, ImageViews and Buttons within CardViews in a RecyclerView.

#### Tutorials:
The tutorials activity will contain YouTube videos that displays step-by-step tutorials on how to trade cryptocurrency, how to make a profit, etc. The page will receive data from the [YouTube](https://developers.google.com/youtube) API, displaying videos within a in a RecyclerView. TextViews will be used as video Titles.

#### News:
The news activity will help users gain knowledge on what is going on in the Crypto world. The news activity layout will have CardViews for each News article which will be in a RecyclerView. To retreive news and articles, we will be using the [CoinDesk](https://www.coindesk.com/api) API. Users will receive notifications which they can enable or disable in Settings.

#### Bitcoin ATM Finder:
This activity will aid users in finding the nearest Bitcoin ATM depending on their location. The [Google Maps](https://cloud.google.com/maps-platform/) API and template will be used.

#### Settings
The settings activity will contain two toggle switches: the first to enable or disable email notifications and the second to change the the theme of the application to Dark mode. A spinner will be used to change the currency the prices will be displayed in. There will also be buttons linking to About Us, Terms of Service, Privacy Policy and a link to the BlockChain Sparrows Website.


### Installation
---

Clone the repository
<br>
```
https://framagit.org/1753475/blockchain-sparrows.git
```


### Roadmap
---
> **1st Milestone**: Design main layouts for each activities + main functions for Tutorials activity.

> **2nd Milestone**: Code functionalities for Coin Price, Announcements, Signal Subscriptions, Login Screen + Register activities and Buy Cryptocurrency Activity

> **3rd Milestone**: Code functionalities for Settings, Bitcoin ATM Finder, Tutorials activity and Crypto News activity
  

### Authors:
---
* George Christeas
* Mohamed Marzouk
* Zubair Janjua


### License
---
This project is licensed under the MIT License. See the [license file](LICENSE) for details.