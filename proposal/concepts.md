## List of core concepts
- [x] Explicit intents 
- [x] Implicit intents (sending and receiving)
- [x] Passing data between activities
- [x] Getting data back from activity
- [x] Ancestral (up) and lateral navigation
- [x] Saving activity state on configuration change
- [x] Buttons, FABs and clickable images
- [x] Input controls
- [x] Menus
- [ ] Pickers
- [x] Dialogs
- [x] Fragments
- [x] RecyclerView
- [x] Tab navigation
- [x] AsyncTask & AsyncTaskLoader
- [x] Internet connection
- [ ] Broadcasts
- [x] Notifications
- [ ] Alarms
- [ ] Data storage in file
- [x] Shared preferences
- [ ] SQLite
- [x] Cloud backup
- [x] App settings
- [ ] Room, LiveData and ViewModel
- [x] Firebase

## Activities Description

### First activity Log-In (Launcher Activity) + Main Menu + Register
The first screen for thr application will be a login screen where a username and password will be authenticated using Google's Firebase database. 
User will need to enter a username and password in order to access the application features. 
User will be able to register a username and a password that will be registered within the database. For navigation, the app will use the drawer navigation to redirect the user to different activities within the application.

### Buy Crypto:
This activity will enable users to buy cryptocurrency. 
Users will be able to choose the desired coins via a dropdown spinner, the page contains TextViews for prices and quantity, and a Button that gives you three options: One for banking information, one for the companys customer support, last lastly, too add bitcoin account number. 
This address will be saved within the firebase database.

### Signal Subscription:
In this activity, the user will be able to subscribe by directing the user directly to the BlockChain Sparrows's service on the Zignaly bot. 
The layout will contain CardViews within a RecyclerView and will contain buttons that enable the subscriptions. 
When the user clicks the button for Academy CardView, users will subscribe to Blockchain Sparrows YouTube channel. 
The layout also contains ImageViews and TextViews.

### Announcements:
The annoucements activity is like the news activity, but users will receive annoucements directly from the Blockchain Sparrows API. 
The layout will contain CardViews within a RecyclerView.

### Coin Prices:
For the prices activity, the application will be receiving data from the CoinMarketCap API to display coin names, prices, percentages, etc. 
This activity's layout will contain TextViews, ImageViews and Buttons within CardViews in a RecyclerView.

### Tutorials:
The tutorials activity will contain YouTube videos that displays step-by-step tutorials on how to trade cryptocurrency, how to make a profit, etc. 
The page will receive data from the YouTube API, displaying videos within a in a RecyclerView. TextViews will be used as video Titles.

### News:
The news activity will help users gain knowledge on what is going on in the Crypto world. 
The news activity layout will have CardViews for each News article which will be in a RecyclerView. 
To retreive news and articles, we will be using the CoinDesk API. Users will receive notifications which they can enable or disable in Settings.

### Bitcoin ATM Finder:
This activity will aid users in finding the nearest Bitcoin ATM depending on their location. The Google Maps API and template will be used.

### Settings
The settings activity will contain two toggle switches: the first to enable or disable email notifications and the second to change the the theme of the application to Dark mode. 
A spinner will be used to change the currency the prices will be displayed in. 
There will also be buttons linking to About Us, Terms of Service, Privacy Policy and a link to the BlockChain Sparrows Website.