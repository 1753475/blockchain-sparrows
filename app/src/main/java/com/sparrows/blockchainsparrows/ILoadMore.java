package com.sparrows.blockchainsparrows;

public interface ILoadMore {
    void onLoadMore();
}
