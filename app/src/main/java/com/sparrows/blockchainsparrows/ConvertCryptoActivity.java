package com.sparrows.blockchainsparrows;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ConvertCryptoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemSelectedListener {

    private static final String LOG_TAG = LoginActivity.class.getSimpleName();
    private FirebaseAuth mAuth;
    public SharedPref sharedPref;
    private TextView value;
    private Spinner coinSpiner;
    private EditText quantity;
    private TextView  mLogout, mNavName, mNavEmail;
    private NavigationView navigationView;
    private double mCoinPrice, coinQuantity, result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPref = new SharedPref(this);

        if(sharedPref.loadDarkModeState()== true)
        {
            setTheme(R.style.darktheme);
        }
        else setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convert_crypto);
        Log.i(LOG_TAG, "=--------- onCreate ---------=");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        navigationView =  findViewById(R.id.nav_view);
        mNavName = navigationView.getHeaderView(0).findViewById(R.id.nav_user_name);
        mNavEmail = navigationView.getHeaderView(0).findViewById(R.id.nav_user_email);
        mLogout = findViewById(R.id.logout);

        loadUserInformation();

        // calculator
        quantity = findViewById(R.id.quantity);
        value = findViewById(R.id.value);

        //coin spinner
        coinSpiner = findViewById(R.id.coinSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.coins, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        coinSpiner.setAdapter(adapter);
        coinSpiner.setOnItemSelectedListener(this);

        //Navigation Drawer
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.action_convert);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_prices:
                        Intent intent1 = new Intent(ConvertCryptoActivity.this, PricesActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.action_tutorials:
                        Intent intent2 = new Intent(ConvertCryptoActivity.this, TutorialsActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.action_news:
                        Intent intent3 = new Intent(ConvertCryptoActivity.this, NewsActivity.class);
                        startActivity(intent3);
                        break;
                }
                return true;
            }
        });

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(ConvertCryptoActivity.this, LoginActivity.class));
            }
        });
    }

    public void loadUserInformation(){
        FirebaseUser user = mAuth.getCurrentUser();

        if(user != null){
            if(user.getDisplayName() != null)
                mNavName.setText(user.getDisplayName());

            if(user.getEmail() != null)
                mNavEmail.setText(user.getEmail());
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_announcements) {
            Intent intent = new Intent(this, AnnouncementsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_atmfinder) {
            Intent intent = new Intent(this, ATMFinderActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_signals) {
            Intent intent = new Intent(this, SignalsSubscribtionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Please Download Blockchain Sparrows App form this link: https://www.blockchainsparrows.com/";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //spiner onItemSelected
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 1:
                mCoinPrice = 7729.06;
                value.setVisibility(View.GONE);
                break;
            case 2:
                mCoinPrice = 155.89;
                value.setVisibility(View.GONE);
                break;
            case 3:
                mCoinPrice = 0.9959;
                value.setVisibility(View.GONE);
                break;
            default:
                mCoinPrice = 0;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void convert(View view) {
        if(mCoinPrice == 0){
            value.setText("Please choose a coin");
            value.setTextColor(Color.parseColor("#FF0000"));
        }else if(quantity.getText().toString().isEmpty()){
            quantity.setError("Please enter a quantity");
            quantity.requestFocus();
        }else{
            value.setVisibility(View.VISIBLE);
            coinQuantity = Double.parseDouble(quantity.getText().toString());
            result = coinQuantity * mCoinPrice;
            value.setText("The Amount is: $" + result);
        }
    }
}
