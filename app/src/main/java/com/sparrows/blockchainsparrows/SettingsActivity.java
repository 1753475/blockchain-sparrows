package com.sparrows.blockchainsparrows;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class SettingsActivity extends AppCompatActivity {

    private static final String LOG_TAG = LoginActivity.class.getSimpleName();
    private FirebaseAuth mAuth;

    public RadioGroup mRadioGroup;
    public RadioButton mUSDRadioButton, mCADRadioButton;
    public Switch mySwitch;
    public SharedPref sharedPref;
    public TextView mDeleteAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        sharedPref = new SharedPref(this);
        if(sharedPref.loadDarkModeState()== true)
        {
            setTheme(R.style.darktheme);
        }
        else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mRadioGroup = findViewById(R.id.currency_choices);
        mUSDRadioButton = mRadioGroup.findViewById(R.id.usd_radio_button);
        mCADRadioButton = mRadioGroup.findViewById(R.id.cad_radio_button);
        mySwitch = findViewById(R.id.myswitch);
        mDeleteAccount = findViewById(R.id.delete_account_textview);

        if (sharedPref.loadCurrencyChoice().equals("CAD"))
        {
            mRadioGroup.check(R.id.cad_radio_button);

        }
        if (sharedPref.loadDarkModeState()== true)
        {
            mySwitch.setChecked(true);

        }
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.usd_radio_button) {
                    sharedPref.setCurrencyChoice("USD");
                    recreate();
                } else if (i == R.id.cad_radio_button) {
                    sharedPref.setCurrencyChoice("CAD");
                    recreate();
                }
            }
        });

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    sharedPref.setDarkModeState(true);
                    recreate();
                }

                else{
                    sharedPref.setDarkModeState(false);
                    recreate();
                }
            }
        });

        mDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(SettingsActivity.this);
                dialog.setIcon(R.drawable.ic_delete);
                dialog.setTitle("Are you sure?");
                dialog.setMessage("Deleting your user account cannot be reversed.");
                dialog.setPositiveButton("Delete Account", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mAuth.getCurrentUser().delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Log.d(LOG_TAG, "Successfully deleted user.");
                                    finish();
                                    Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                                else{
                                    Log.d(LOG_TAG, task.getException().getMessage());
                                }
                            }
                        });
                    }
                });
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.i(LOG_TAG, "=--- AlertDialog Cancelled ---=");
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog alert = dialog.create();
                alert.show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void AboutUsOnClick(View view) {
        String url = "https://www.blockchainsparrows.com/signals/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
