package com.sparrows.blockchainsparrows.models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CoinModel {
    @SerializedName("cmc_rank")
    @Expose
    private String rank;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("price")
    @Expose
    private String price_usd;
    @SerializedName("market_cap")
    @Expose
    private String market_cap_usd;
    @SerializedName("percent_change_24h")
    @Expose
    private String percent_change_24h;

    public CoinModel() {
    }

    public CoinModel(String rank, String id, String name, String symbol, String price_usd, String market_cap_usd, String percent_change_24h) {
        this.rank = rank;
        this.id = id;
        this.name = name;
        this.symbol = symbol;
        this.price_usd = price_usd;
        this.market_cap_usd = market_cap_usd;
        this.percent_change_24h = percent_change_24h;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getPrice() {
        return price_usd;
    }

    public void setPrice(String price_usd) {
        this.price_usd = price_usd;
    }

    public String getMarket_cap_usd() {
        return market_cap_usd;
    }

    public void setMarket_cap_usd(String market_cap_usd) {
        this.market_cap_usd = market_cap_usd;
    }

    public String getPercent_change_24h() {
        return percent_change_24h;
    }

    public void setPercent_change_24h(String percent_change_24h) { this.percent_change_24h = percent_change_24h; }

    @NonNull
    @Override
    public String toString() {
        return getRank() + ", " + getName() + ", " +  getSymbol() + ", " +  getPrice() + ", " +  getMarket_cap_usd() + ", " +  getPercent_change_24h();
    }
}
