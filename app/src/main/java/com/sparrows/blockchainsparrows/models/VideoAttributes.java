package com.sparrows.blockchainsparrows.models;

public class VideoAttributes
{
    private String title;
    private String media_url;
    private String duration;




    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }



    public VideoAttributes(String title, String media_url, String duration) {
        this.title = title;
        this.media_url = media_url;
        this.duration = duration;

    }



}
