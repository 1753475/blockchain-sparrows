package com.sparrows.blockchainsparrows;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import static android.content.Context.MODE_PRIVATE;

public class SharedPref
{
    SharedPreferences mySharedPref;

    public SharedPref(Context context)
    {
        mySharedPref = context.getSharedPreferences("filename", MODE_PRIVATE);
    }

    public void setCurrencyChoice(String choice)
    {
        SharedPreferences.Editor editor = mySharedPref.edit();
        editor.putString("Currency", choice);
        editor.commit();
    }

    public String loadCurrencyChoice()
    {
        String choice = mySharedPref.getString("Currency", "USD");
        return choice;
    }

    public void setDarkModeState(Boolean state)
    {
        SharedPreferences.Editor editor = mySharedPref.edit();
        editor.putBoolean("DarkMode",state);
        editor.commit();
    }

    public Boolean loadDarkModeState()
    {
        Boolean state = mySharedPref.getBoolean("DarkMode",false);
        return state;
    }
}
