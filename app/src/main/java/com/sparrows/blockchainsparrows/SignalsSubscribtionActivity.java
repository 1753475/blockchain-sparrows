package com.sparrows.blockchainsparrows;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class SignalsSubscribtionActivity extends AppCompatActivity {
    public SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPref = new SharedPref(this);

        if(sharedPref.loadDarkModeState()== true)
        {
            setTheme(R.style.darktheme);
        }
        else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signals_subscribtion);
    }

    public void FreeSignals(View view) {
        Uri free = Uri.parse("https://www.blockchainsparrows.com//");
        Intent webIntent = new Intent(Intent.ACTION_VIEW, free);
        startActivity(webIntent);

    }
    public void SilverSubscribtion(View view) {
        Uri silver = Uri.parse("https://zignaly.com/app/providers/5d47fd846c20cd2e6f1a2343/");
        Intent webIntent = new Intent(Intent.ACTION_VIEW, silver);
        startActivity(webIntent);

    }
    public void GoldSubscribtion(View view) {
        Uri telegramSupport = Uri.parse("https://t.me/Sparrows_Support");
        Intent webIntent = new Intent(Intent.ACTION_VIEW, telegramSupport);
        startActivity(webIntent);

    }
    public void AcademySubscribtion(View view) {
        Uri acadmychannel = Uri.parse("https://www.youtube.com/watch?v=GUL3NFIlJb0&list=PLm7kFBm_Q6mlhQmd-5GJzV5RmRq9lEXR1");
        Intent webIntent = new Intent(Intent.ACTION_VIEW, acadmychannel);
        startActivity(webIntent);

    }
}

