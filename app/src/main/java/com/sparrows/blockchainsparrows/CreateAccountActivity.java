package com.sparrows.blockchainsparrows;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class CreateAccountActivity extends AppCompatActivity{

    private static final String LOG_TAG = CreateAccountActivity.class.getSimpleName();
    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";
    private static final int NOTIFICATION_ID = 0;

    private FirebaseAuth mAuth;
    private NotificationManager mNotifyManager;

    private EditText mFullName, mEmailField, mPasswordField, mPassword2Field;
    private TextView mAgreement;
    private Button createAccountButton;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_account);
        setContentView(R.layout.activity_create_account);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        mFullName = findViewById(R.id.fullname);
        mEmailField = findViewById(R.id.email_signup);
        mPasswordField = findViewById(R.id.password_signup);
        mPassword2Field = findViewById(R.id.password2);
        createAccountButton = findViewById(R.id.create_account_button);
        progressBar = findViewById(R.id.progress_bar_create_account);
        mAgreement = findViewById(R.id.terms_conditions);

        mAgreement.setText(Html.fromHtml(
                "By Creating an Account, you agree to our " +
                        "<a href=\"https://www.blockchainsparrows.com/\">Terms of Service</a> " +
                        "and <a href=\"https://www.blockchainsparrows.com/\">Privacy Policy</a>."
        ));
        mAgreement.setMovementMethod(LinkMovementMethod.getInstance());

        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount();
            }
        });
        createNotificationChannel();
    }

    public void createNotificationChannel()
    {
        mNotifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(PRIMARY_CHANNEL_ID,
                    "New User Notification", NotificationManager
                    .IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription("New User Notification");
            mNotifyManager.createNotificationChannel(notificationChannel);
        }
    }

    private NotificationCompat.Builder getNotificationBuilder(){
        Intent notificationIntent = new Intent(this, CreateAccountActivity.class);
        PendingIntent notificationPendingIntent = PendingIntent.getActivity(this,
                NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID)
                .setContentTitle("Welcome!")
                .setContentText(mFullName.getText().toString() + ", welcome to Blockchain Sparrows!")
                .setSmallIcon(R.drawable.ic_price)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setContentIntent(notificationPendingIntent)
                .setAutoCancel(true);
        return notifyBuilder;
    }

    private void sendNotification() {
        NotificationCompat.Builder notifyBuilder = getNotificationBuilder();
        mNotifyManager.notify(NOTIFICATION_ID, notifyBuilder.build());
    }

    /**
     * Method to create a new user account
     */
    private void createAccount() {
        final String fullname = mFullName.getText().toString();
        String email = mEmailField.getText().toString().trim();
        String password = mPasswordField.getText().toString().trim();
        String password2 = mPassword2Field.getText().toString().trim();

        if(fullname.isEmpty()){
            mFullName.setError("Name required");
            mFullName.requestFocus();
            return;
        }
        if(!fullname.matches("^[a-zA-Z]*$")){
            mFullName.setError("Only letters and space allowed");
            mFullName.requestFocus();
            return;
        }
        if(email.isEmpty()){
            mEmailField.setError("Email required");
            mEmailField.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            mEmailField.setError("Enter valid email");
            mEmailField.requestFocus();
            return;
        }
        if(password.isEmpty()){
            mPasswordField.setError("Password Required");
            mPasswordField.requestFocus();
            return;
        }
        if(password.length() < 6){
            mPasswordField.setError("6 characters required");
            mPasswordField.requestFocus();
            return;
        }
        if(!password2.equals(password)){
            mPasswordField.setError("Passwords to not match");
            mPassword2Field.setError("Passwords to not match");
            mPassword2Field.requestFocus();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);
                if(task.isSuccessful()){
                    // Sign in is successful
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(fullname).build();
                    user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.d(LOG_TAG, "User profile updated.");
                            }
                        }
                    });
                    sendNotification();
                    finish();
                    Intent intent = new Intent(CreateAccountActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.GONE);
                if (e instanceof FirebaseAuthException) {
                    ((FirebaseAuthException) e).getErrorCode();
                    Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
