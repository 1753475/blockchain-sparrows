package com.sparrows.blockchainsparrows;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.widget.TextView;

import static com.google.android.material.bottomnavigation.LabelVisibilityMode.LABEL_VISIBILITY_UNLABELED;

public class AnnouncementsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;
    private TextView mLogout, mNavName, mNavEmail, mMoreInfo1, mMoreInfo2;
    private NavigationView navigationView;
    public SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new SharedPref(this);
        if(sharedPref.loadDarkModeState()== true)
        {
            setTheme(R.style.darktheme);
            //navigationView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        else setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_announcements);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigationView =  findViewById(R.id.nav_view);
        mNavName = navigationView.getHeaderView(0).findViewById(R.id.nav_user_name);
        mNavEmail = navigationView.getHeaderView(0).findViewById(R.id.nav_user_email);
        mLogout = findViewById(R.id.logout);
        mMoreInfo1 = findViewById(R.id.more_info_1);
        mMoreInfo2 = findViewById(R.id.more_info_2);

        loadUserInformation();

        mMoreInfo1.setMovementMethod(LinkMovementMethod.getInstance());
        mMoreInfo2.setMovementMethod(LinkMovementMethod.getInstance());

        //Navigation Drawer
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setLabelVisibilityMode(LABEL_VISIBILITY_UNLABELED);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_prices:
                        Intent intent1 = new Intent(AnnouncementsActivity.this, PricesActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.action_convert:
                        Intent intent2 = new Intent(AnnouncementsActivity.this, ConvertCryptoActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.action_tutorials:
                        Intent intent3 = new Intent(AnnouncementsActivity.this, TutorialsActivity.class);
                        startActivity(intent3);
                        break;
                    case R.id.action_news:
                        Intent intent4 = new Intent(AnnouncementsActivity.this, NewsActivity.class);
                        startActivity(intent4);
                        break;
                }
                return true;
            }
        });

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(AnnouncementsActivity.this, LoginActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void loadUserInformation(){
        FirebaseUser user = mAuth.getCurrentUser();

        if(user != null){
            if(user.getDisplayName() != null)
                mNavName.setText(user.getDisplayName());

            if(user.getEmail() != null)
                mNavEmail.setText(user.getEmail());
        }
        if(sharedPref.loadDarkModeState()== true)
            navigationView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_announcements) {
            Intent intent = new Intent(this, AnnouncementsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_atmfinder) {
            Intent intent = new Intent(this, ATMFinderActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_signals) {
            Intent intent = new Intent(this, SignalsSubscribtionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Please Download Blockchain Sparrows App form this link: https://www.blockchainsparrows.com/";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
