package com.sparrows.blockchainsparrows;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.sparrows.blockchainsparrows.adapter.YoutubeVideoAdapter;
import com.sparrows.blockchainsparrows.models.VideoAttributes;

import java.util.ArrayList;

public class TutorialsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String LOG_TAG = PricesActivity.class.getSimpleName();
    private FirebaseAuth mAuth;

    private RecyclerView recyclerView;
    private TextView mLogout, mNavName, mNavEmail;
    private NavigationView navigationView;
    public SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        sharedPref = new SharedPref(this);

        if(sharedPref.loadDarkModeState()== true)
        {
            setTheme(R.style.darktheme);
        }
        else setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorials);
        Log.i(LOG_TAG, "=--------- onCreate ---------=");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        navigationView =  findViewById(R.id.nav_view);
        mNavName = navigationView.getHeaderView(0).findViewById(R.id.nav_user_name);
        mNavEmail = navigationView.getHeaderView(0).findViewById(R.id.nav_user_email);
        mLogout = findViewById(R.id.logout);

        loadUserInformation();

        //Navigation Drawer
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.action_tutorials);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_prices:
                        Intent intent1 = new Intent(TutorialsActivity.this, PricesActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.action_convert:
                        Intent intent2 = new Intent(TutorialsActivity.this, ConvertCryptoActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.action_news:
                        Intent intent3 = new Intent(TutorialsActivity.this, NewsActivity.class);
                        startActivity(intent3);
                        break;
                }
                return true;
            }
        });

        recyclerView = findViewById(R.id.youtube_recycler);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(TutorialsActivity.this, LoginActivity.class));
            }
        });

        final ArrayList<VideoAttributes> youtubeVideoModelArrayList = generateDummyVideoList();
        YoutubeVideoAdapter adapter = new YoutubeVideoAdapter(this, youtubeVideoModelArrayList);
        recyclerView.setAdapter(adapter);


        //set click event
        recyclerView.addOnItemTouchListener(new RecyclerViewOnClickListener(this, new RecyclerViewOnClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                startActivity(new Intent(TutorialsActivity.this, YoutubePlayerActivity.class)
                       .putExtra("video_id", youtubeVideoModelArrayList.get(position).getMedia_url()));
            }
        }));
    }

    private ArrayList<VideoAttributes> generateDummyVideoList() {
        ArrayList<VideoAttributes> youtubeVideoModelArrayList = new ArrayList<>();

        //get the video id array, title array and duration array from strings.xml
        String[] videoIDArray = getResources().getStringArray(R.array.video_id_array);
        String[] videoTitleArray = getResources().getStringArray(R.array.video_title_array);
        String[] videoDurationArray = getResources().getStringArray(R.array.video_duration_array);

        //loop through all items and add them to arraylist
        for (int i = 0; i < videoIDArray.length; i++) {

            VideoAttributes youtubeVideoModel = new VideoAttributes("z","z","z");
            youtubeVideoModel.setMedia_url(videoIDArray[i]);
            youtubeVideoModel.setTitle(videoTitleArray[i]);
            youtubeVideoModel.setDuration(videoDurationArray[i]);

            youtubeVideoModelArrayList.add(youtubeVideoModel);

        }
        return youtubeVideoModelArrayList;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void loadUserInformation(){
        FirebaseUser user = mAuth.getCurrentUser();

        if(user != null){
            if(user.getDisplayName() != null)
                mNavName.setText(user.getDisplayName());

            if(user.getEmail() != null)
                mNavEmail.setText(user.getEmail());
        }
        if(sharedPref.loadDarkModeState()== true)
            navigationView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_announcements) {
            Intent intent = new Intent(this, AnnouncementsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_atmfinder) {
            Intent intent = new Intent(this, ATMFinderActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_signals) {
            Intent intent = new Intent(this, SignalsSubscribtionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Please Download Blockchain Sparrows App form this link: https://www.blockchainsparrows.com/";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
