package com.sparrows.blockchainsparrows;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sparrows.blockchainsparrows.adapter.CoinAdapter;
import com.sparrows.blockchainsparrows.models.CoinModel;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class PricesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private static final String LOG_TAG = PricesActivity.class.getSimpleName();
    private static final String API_KEY = "b1a7a551-a147-4b48-b078-4bc172cf514b";
    private static final String API_KEY2 = "36be91ca-479d-4997-8972-627dec048bc9";

    private FirebaseAuth mAuth;

    private View parentLayout;
    private TextView mLogout, mNavName, mNavEmail;
    private NavigationView navigationView;
    public SharedPref sharedPref;
    private List<CoinModel> mItems = new ArrayList<>();
    private CoinAdapter mAdapter;
    private RecyclerView mRecyclerView;

    private OkHttpClient mClient;
    private Request mRequest;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new SharedPref(this);

        if(sharedPref.loadDarkModeState()== true)
        {
            setTheme(R.style.darktheme_prices);
        }
        else setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_prices);
        Log.i(LOG_TAG, "=--------- onCreate ---------=");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        navigationView =  findViewById(R.id.nav_view);
        mNavName = navigationView.getHeaderView(0).findViewById(R.id.nav_user_name);
        mNavEmail = navigationView.getHeaderView(0).findViewById(R.id.nav_user_email);

        mProgressBar = findViewById(R.id.progress_bar_prices);
        mSwipeRefreshLayout = findViewById(R.id.swipe_layout);
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                loadFirst10Coins(1);
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mItems.clear();
                loadFirst10Coins(1);
                setupAdapter();
            }
        });
        mRecyclerView = findViewById(R.id.coin_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        setupAdapter();
        parentLayout = findViewById(android.R.id.content);
        mLogout = findViewById(R.id.logout);

        loadUserInformation();

        //Navigation Drawer
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            // Create background thread to connect and get data
        } else {
            Snackbar.make(parentLayout, "No network connection available.", Snackbar.LENGTH_LONG).show();
        }

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_convert:
                        Intent intent1 = new Intent(PricesActivity.this, ConvertCryptoActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.action_tutorials:
                        Intent intent2 = new Intent(PricesActivity.this, TutorialsActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.action_news:
                        Intent intent3 = new Intent(PricesActivity.this, NewsActivity.class);
                        startActivity(intent3);
                        break;
                }
                return true;
            }
        });

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(PricesActivity.this, LoginActivity.class));
            }
        });
    }

    private void setupAdapter() {
        mAdapter = new CoinAdapter(mRecyclerView, PricesActivity.this, mItems);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setiLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                if(mItems.size() <= 1000){ // Max size is 1000 coin
                    if (mItems.size() > 0)
                        loadNext10Coins(mItems.size());
                    else
                        Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_LONG).show();
                }
                else
                    Toast.makeText(PricesActivity.this, "Reached Limit", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadFirst10Coins(int index) {

        HttpLoggingInterceptor interceptor  = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);

        HttpUrl url = new HttpUrl.Builder()
                .scheme("https")
                .host("pro-api.coinmarketcap.com")
                .addPathSegment("/v1/cryptocurrency/listings/latest")
                .addQueryParameter("start", String.valueOf(index))
                .addQueryParameter("limit", "1000")
                .addQueryParameter("convert", "USD")
                .build();

        mClient = new OkHttpClient().newBuilder().addInterceptor(interceptor ).build();
        mRequest = new Request.Builder().url(url)
                .header("Accept", "application/json")
                .addHeader("X-CMC_PRO_API_KEY", API_KEY)
                .build();

        mClient.newCall(mRequest).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.d(LOG_TAG, e.getMessage());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                Log.d(LOG_TAG, "CODE: " + response.code());
                Log.d(LOG_TAG, response.message());

                String BODY = response.body().string();
                final List<CoinModel> newCoins = parseDataResponse(BODY);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setVisibility(View.GONE);
                        mItems.addAll(newCoins);
                        mAdapter.updateData(newCoins);
                        Log.d(LOG_TAG, newCoins.toString());
                    }
                });
            }
        });
        if(mSwipeRefreshLayout.isRefreshing()){
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private List<CoinModel> parseDataResponse(String body){
        List<CoinModel> newCoins = new ArrayList<CoinModel>();
        try {
            JSONObject jsonObject = new JSONObject(body);
            JSONArray dataArray = jsonObject.getJSONArray("data");
            for(int i = 0; i < 10; i++) {
                JSONObject dataObject = dataArray.getJSONObject(i);
                JSONObject priceData = dataObject.getJSONObject("quote").getJSONObject("USD");
                String rank = dataObject.getString("cmc_rank");
                String id = dataObject.getString("id");
                String name = dataObject.getString("name");
                String symbol = dataObject.getString("symbol");
                String price_usd = priceData.getString("price");
                String percent_change_24h = priceData.getString("percent_change_24h");
                String market_cap = priceData.getString("market_cap");
                CoinModel coin = new CoinModel(rank, id,  name, symbol, price_usd, market_cap, percent_change_24h);
                newCoins.add(coin);
            }

        }catch (JSONException e) {
            e.printStackTrace();
        }
        return newCoins;
    }

    private void loadNext10Coins(int index) {

        HttpLoggingInterceptor interceptor  = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);

        HttpUrl url = new HttpUrl.Builder()
                .scheme("https")
                .host("pro-api.coinmarketcap.com")
                .addPathSegment("/v1/cryptocurrency/listings/latest")
                .addQueryParameter("start", String.valueOf(index))
                .addQueryParameter("limit", "1000")
                .addQueryParameter("convert", "USD")
                .build();

        mClient = new OkHttpClient().newBuilder().addInterceptor(interceptor).build();
        mRequest = new Request.Builder().url(url)
                .header("Accept", "application/json")
                .addHeader("X-CMC_PRO_API_KEY", API_KEY)
                .build();

        mClient.newCall(mRequest).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Toast.makeText(PricesActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                Log.d(LOG_TAG, "CODE: " + response.code());
                Log.d(LOG_TAG, response.message());

                String BODY = response.body().string();
                final List<CoinModel> newCoins = parseDataResponse(BODY);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setVisibility(View.GONE);
                        mItems.addAll(newCoins);
                        mAdapter.setLoaded();
                        mAdapter.updateData(mItems);
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        });
        if(mSwipeRefreshLayout.isRefreshing()){
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void loadUserInformation(){
        FirebaseUser user = mAuth.getCurrentUser();

        if(user != null){
            if(user.getDisplayName() != null)
                mNavName.setText(user.getDisplayName());

            if(user.getEmail() != null)
                mNavEmail.setText(user.getEmail());
        }
        if(sharedPref.loadDarkModeState()== true)
            navigationView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_announcements) {
            Intent intent = new Intent(this, AnnouncementsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_atmfinder) {
            Intent intent = new Intent(this, ATMFinderActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_signals) {
            Intent intent = new Intent(this, SignalsSubscribtionActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Please Download Blockchain Sparrows App form this link: https://www.blockchainsparrows.com/";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
