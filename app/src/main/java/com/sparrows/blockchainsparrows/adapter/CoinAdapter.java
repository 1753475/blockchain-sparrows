package com.sparrows.blockchainsparrows.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sparrows.blockchainsparrows.ILoadMore;
import com.sparrows.blockchainsparrows.R;
import com.sparrows.blockchainsparrows.models.CoinModel;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

public class CoinAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ILoadMore iLoadMore;
    private boolean isLoading;
    private Activity mActivity;
    private List<CoinModel> mItems;

    private int visibleThreshold = 5, lastVisibleItem, totalItemCount;

    public CoinAdapter(RecyclerView recyclerView, Activity activity, List<CoinModel> items) {
        mActivity = activity;
        mItems = items;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (iLoadMore != null) {
                        iLoadMore.onLoadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    public void setiLoadMore(ILoadMore iLoadMore) {
        this.iLoadMore = iLoadMore;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.coin_card, parent, false);
        return new CoinViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CoinModel coin = mItems.get(position);
        CoinViewHolder holderItem = (CoinViewHolder) holder;

        holderItem.mCoinSymbol.setText("(" + coin.getSymbol() + ")");
        holderItem.mCoinName.setText(coin.getName().trim());
        holderItem.mCoinPrice.setText(currencyFormat(Double.parseDouble(coin.getPrice())));
        holderItem.mCoinMarketCap.setText(currencyFormat2(Double.parseDouble(coin.getMarket_cap_usd())));
        holderItem.mCoinPercentage24h.setText(coin.getPercent_change_24h() + " %");

        //Load Image
        String url = "https://s2.coinmarketcap.com/static/img/coins/64x64/" + coin.getId() + ".png";
        Picasso.get().load(url)
                .error(R.mipmap.ic_launcher)
                .into(((CoinViewHolder) holder).mCoinIcon);

        holderItem.mCoinPercentage24h.setTextColor(coin.getPercent_change_24h().contains("-") ?
             Color.parseColor("#FF0000") : Color.parseColor("#32CD32"));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void updateData(List<CoinModel> coinModels){
        this.mItems = coinModels;
        notifyDataSetChanged();
    }

    public class CoinViewHolder extends RecyclerView.ViewHolder {

        private ImageView mCoinIcon;
        private TextView mCoinSymbol, mCoinName, mCoinPrice, mCoinMarketCap, mCoinPercentage24h;

        public CoinViewHolder(@NonNull View itemView) {
            super(itemView);

            mCoinIcon = itemView.findViewById(R.id.coin_icon);
            mCoinSymbol = itemView.findViewById(R.id.coin_symbol);
            mCoinName = itemView.findViewById(R.id.coin_name);
            mCoinPrice = itemView.findViewById(R.id.coin_price);
            mCoinMarketCap = itemView.findViewById(R.id.coin_market_cap);
            mCoinPercentage24h = itemView.findViewById(R.id.percent_change_24h);
        }
    }

    /**
     * Method to format double into currency string
     *
     * @param amount Double to be formatted
     * @return Returns formatted String
     */
    public static String currencyFormat(Double amount) {
        DecimalFormat formatter = new DecimalFormat("$###,###,##0.0000");
        return formatter.format(amount);
    }

    /**
     * Method to format double into currency string
     *
     * @param amount Double to be formatted
     * @return Returns formatted String
     */
    public static String currencyFormat2(Double amount) {
        DecimalFormat formatter = new DecimalFormat("$###,###,###,##0");
        return formatter.format(amount);
    }
}

