package com.sparrows.blockchainsparrows.adapter;

import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sparrows.blockchainsparrows.R;

public class LoadingViewHolder extends RecyclerView.ViewHolder  {

    private ProgressBar mProgressBar;

    public LoadingViewHolder(@NonNull View itemView) {
        super(itemView);

        mProgressBar = itemView.findViewById(R.id.progress_bar_prices);
    }
}
