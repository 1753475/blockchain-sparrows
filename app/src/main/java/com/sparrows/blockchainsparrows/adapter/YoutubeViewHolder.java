package com.sparrows.blockchainsparrows.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.youtube.player.YouTubeThumbnailView;
import com.sparrows.blockchainsparrows.R;

public class YoutubeViewHolder extends RecyclerView.ViewHolder {
    public YouTubeThumbnailView videoThumbnailImageView;
    public TextView videoTitle, videoDuration;

    public YoutubeViewHolder(View itemView) {
        super(itemView);
        videoThumbnailImageView = itemView.findViewById(R.id.video_thumbnail_image_view);
        videoTitle = itemView.findViewById(R.id.video_title_youtube);
        videoDuration = itemView.findViewById(R.id.video_duration);
    }
}