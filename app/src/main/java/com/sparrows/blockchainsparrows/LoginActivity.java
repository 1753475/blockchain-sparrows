package com.sparrows.blockchainsparrows;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;

public class LoginActivity extends AppCompatActivity implements  View.OnClickListener {

    private static final String LOG_TAG = LoginActivity.class.getSimpleName();

    private FirebaseAuth mAuth;

    private EditText mEmailField, mPasswordField;
    private Button signInButton, createAccountButton;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Log.i(LOG_TAG, "=--------- onCreate ---------=");

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        mEmailField = findViewById(R.id.email_login);
        mPasswordField = findViewById(R.id.password_login);
        signInButton = findViewById(R.id.login_button);
        createAccountButton = findViewById(R.id.create_account_launcher);
        progressBar = findViewById(R.id.progress_bar_login);

        signInButton.setOnClickListener(this);
        createAccountButton.setOnClickListener(this);
        mPasswordField.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)){
                    userLogin();
                    return true;
                }
                return false;
            }
        });
    }

    private void userLogin(){
        String email = mEmailField.getText().toString().trim();
        String password = mPasswordField.getText().toString().trim();

        if(email.isEmpty()){
            mEmailField.setError("Email required");
            mEmailField.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            mEmailField.setError("Enter valid email");
            mEmailField.requestFocus();
            return;
        }
        if(email.isEmpty()){
            mPasswordField.setError("Password Required");
            mPasswordField.requestFocus();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);
                if(task.isSuccessful()){
                    Log.d(LOG_TAG, "signInWithEmail: success");
                    boolean isNew = task.getResult().getAdditionalUserInfo().isNewUser();
                    Log.d(LOG_TAG, "signInWithEmail: " + (isNew ? "New user" : "Old user"));
                    finish();
                    Intent intent = new Intent(LoginActivity.this, PricesActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else{
                    Log.w(LOG_TAG, "signInWithEmail:failure", task.getException());
                }
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.GONE);
                if (e instanceof FirebaseAuthException) {
                    ((FirebaseAuthException) e).getErrorCode();
                    Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mAuth.getCurrentUser() != null){
            finish();
            startActivity(new Intent(this, PricesActivity.class));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_button:
                userLogin();
                break;
            case R.id.create_account_launcher:
                finish();
                Log.d(LOG_TAG, "Create Account Button clicked!");
                Intent intent = new Intent(LoginActivity.this, CreateAccountActivity.class);
                startActivity(intent);
                break;
        }
    }
}
